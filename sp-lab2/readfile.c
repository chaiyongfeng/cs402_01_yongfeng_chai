#include "readfile.h"
#include <stdlib.h>
#include "helper.h"

/* Print the option info */
void print_option_info();

/* Print the database */
void print_the_database(int emp_count, struct Employee employee[MAXEMPLOYEE]);

/* Lookup employee by ID */
void lookup_by_id(int emp_count, int list[MAXEMPLOYEE], struct Employee employee[MAXEMPLOYEE]);

/* Lookup employee by last name */
void lookup_by_lastname(int emp_count, struct Employee employee[MAXEMPLOYEE]);

/* Add an employee */
void add_an_employee(int *emp_count, struct Employee employee[MAXEMPLOYEE], int list[MAXEMPLOYEE]);

/* Remove an employee by id */
void remove_an_employee(int *emp_count, struct Employee employee[MAXEMPLOYEE], int list[MAXEMPLOYEE]);

/* Update an employee */
void update_an_employee(int emp_count, struct Employee employee[MAXEMPLOYEE], int list[MAXEMPLOYEE]);

/* Print the M employees with the highest salaries */
void print_the_highest_salaries_employee(int emp_count, struct Employee employee[MAXEMPLOYEE]);

/* Find all employees with matching last name */
void find_all_employees_with_matching_last_name(int emp_count, struct Employee employee[MAXEMPLOYEE]);

/* Quit */
void quit();

int main(int argc, char const *argv[]) {
    if (argc < 2) {
        printf("Need pass a filename to read...\n");
        return 0;
    }

    char *fname = argv[1];
    struct Employee employee[MAXEMPLOYEE];
    int emp_count = 0, list[MAXEMPLOYEE];

    if (open_file(fname) == -1) {
        printf("Occur an error when reading this file!");
        return -1;
    }

    printf("\nfname: %s\n", fname);
    FILE *fp = fopen(fname, "r");
    while(!feof(fp)) {
        fscanf(fp, "%d %s %s %d\n", &employee[emp_count].id, &employee[emp_count].firstname, &employee[emp_count].lastname, &employee[emp_count].salary);
        list[emp_count] = employee[emp_count].id;
        emp_count++;
    }

    qsort(employee, emp_count, sizeof(struct Employee), compared_by_id);
    qsort(list, emp_count, sizeof(int), compare);

    close_file(fp);

    while(1) {
        print_option_info();
        int option;
        while (read_int(&option) == -1) {
            printf("Invalid input! Please try again: ");
            safe_flush();
        }
        printf("\n");
    
        if (option == 1) {
            print_the_database(emp_count, employee);
        } else if (option == 2) {
            lookup_by_id(emp_count, list, employee);
        } else if (option == 3) {
            lookup_by_lastname(emp_count, employee);
        } else if (option == 4) {
            add_an_employee(&emp_count, employee, list);
        } else if(option == 5) {
            quit();
            break;
        } else if (option == 6) {
            remove_an_employee(&emp_count, employee, list);
        } else if (option == 7) {
            update_an_employee(emp_count, employee, list);
        } else if (option == 8) {
            print_the_highest_salaries_employee(emp_count, employee);
        } else if (option == 9) {
            find_all_employees_with_matching_last_name(emp_count, employee);
        } else {
            printf("Hey, %d is not between 1 and 9, try again...\n", option);
            continue;
        }
    }
}

void print_option_info() {
    printf("\n");
    printf("Employee DB Menu:\n");
    printf("----------------------------------\n");
    printf("  (1) Print the Database\n");
    printf("  (2) Lookup by ID\n");
    printf("  (3) Lookup by Last Name\n");
    printf("  (4) Add an Employee\n");
    printf("  (5) Quit\n");
    printf("  (6) Remove an employee\n");
    printf("  (7) Update an employee's information\n");
    printf("  (8) Print the M employees with the highest salaries\n");
    printf("  (9) Find all employees with matching last name\n");
    printf("----------------------------------\n");
    printf("Enter your choice: ");
}

void print_the_database(int emp_count, struct Employee employee[MAXEMPLOYEE]) {
    printf("\n");
    printf("NAME                              SALARY 	     ID\n");
    printf("---------------------------------------------------------------\n");
    for (int i = 0; i < emp_count; i++) {
        printf("%-15s %-10s %13d %15d \n", employee[i].firstname, employee[i].lastname, employee[i].salary, employee[i].id);
    }
    printf("---------------------------------------------------------------\n");
    printf(" Number of Employees (%d)\n", emp_count);
};

void lookup_by_id(int emp_count, int list[MAXEMPLOYEE], struct Employee employee[MAXEMPLOYEE]) {
    int emp_id, key;
    printf("Enter a 6 digit employee id: ");
    while (read_int(&emp_id) == -1) {
        printf("Invalid input! Please try again: ");
        safe_flush();
    }
    printf("\n");
    key = binary_search(list, 0, emp_count, emp_id);
    if (key == -1) {
        printf("Employee with id %d not found in DB \n", emp_id);
    } else {
        print_by_key(employee, key);
    }
}

void lookup_by_lastname(int emp_count, struct Employee employee[MAXEMPLOYEE]) {
    char lastname[MAXNAME];
    int key;
    printf("Enter Employee's lastname (no extra spaces): ");
    read_string(&lastname);
    printf("\n");
    key = search_by_lastName(employee, emp_count, lastname);
    if (key == -1) {
        printf("Employee with lastname %s not found in DB. \n", lastname);
    } else {
        print_by_key(employee, key);
    }
}

void add_an_employee(int *emp_count, struct Employee employee[MAXEMPLOYEE], int list[MAXEMPLOYEE]) {
    char firstname[MAXNAME], lastname[MAXNAME];
    int salary, is_confirm;
    
    printf("Enter the firstname of the employee: ");
    read_string(&firstname);
    printf("\n");
    printf("Enter the lastname of the employee: ");
    read_string(&lastname);
    printf("\n");
    printf("Enter employee's salary (30000 to 150000): ");
    
    while (read_int(&salary) == -1 || salary < 30000 || salary > 150000) {
        printf("\nSalary you input is invalid. Please enter another salary: ");
        safe_flush();
    }

    printf("\n");
    printf("Do you want to add the following employee to the DB?\n");
    printf("  %s %s, salary: %d\n", firstname, lastname, salary);
    printf("\n");
    printf("Enter 1 for yes, 0 for no: ");
    while (read_int(&is_confirm) == -1) {
        printf("Invalid input! Please try again: ");
        safe_flush();
    }
    printf("\n");
    if (is_confirm) {
        int cur_id = employee[*emp_count - 1].id;
        int new_id = cur_id + 1;
        strcpy(&employee[*emp_count].firstname, firstname);
        strcpy(&employee[*emp_count].lastname, lastname);
        employee[*emp_count].salary = salary;
        employee[*emp_count].id = new_id;
        list[*emp_count] = employee[*emp_count].id;
        *emp_count = *emp_count + 1;
        printf("Added!\n");
    } else {
        printf("Cancelled!.\n");
    }
}

void quit() {
    printf("goodbye!\n");
}

void remove_an_employee(int *emp_count, struct Employee employee[MAXEMPLOYEE], int list[MAXEMPLOYEE]) {
    if (*emp_count == 0) {
        printf("No employee in DB, please add first.\n");
        return;
    }
    printf("⚠️ Warning you are going to delete an employee!\n");
    int emp_id, is_confirm;
    printf("Enter a 6 digit employee id you want to remove: ");
    int result = read_int(&emp_id);
    while (result == -1) {
        printf("Invalid id! Please try again: ");
        safe_flush();
        result = read_int(&emp_id);
    }
    printf("\n");
    
    int index = binary_search(list, 0, *emp_count, emp_id);
    if (index == -1) {
        printf("Employee with id %d not found in DB \n", emp_id);
    } else {
        printf("Are you sure you want to delete the following employee in the DB?\n\n");
        print_by_key(employee, index);
        printf("Enter 1 for yes, 0 for no: ");
        read_int(&is_confirm);
        printf("\n");
        
        if (is_confirm == 0) {
            printf("Don't want to delete the employee in the DB.\n");
            return;
        }
        remove_element(list, index, *emp_count);
        remove_employee(employee, index, *emp_count);
        *emp_count = *emp_count - 1;
        printf("Removed Employee with id %d in DB \n", emp_id);
    }
}

void update_an_employee(int emp_count, struct Employee employee[MAXEMPLOYEE], int list[MAXEMPLOYEE]) {
    int emp_id, is_confirm;
    printf("Enter a 6 digit employee id you want to upate: ");
    int result = read_int(&emp_id);
    while (result == -1) {
        printf("Invalid id! Please try again: ");
        safe_flush();
        result = read_int(&emp_id);
    }
    printf("\n");
    
    int index = binary_search(list, 0, emp_count, emp_id);
    if (index == -1) {
        printf("Employee with id %d not found in DB \n", emp_id);
    } else {
        printf("You are going to upadte the following employee in the DB?\n\n");
        print_by_key(employee, index);
        
        char first_name[MAXNAME], last_name[MAXNAME];
        int salary;
        
        printf("Enter the updated first name of the employee: ");
        read_string(&first_name);
        printf("Enter the updated last name of the employee: ");
        read_string(&last_name);
        printf("Enter the updated employee's salary (30000 to 150000): ");
        
        while (read_int(&salary) == -1 || salary < 30000 || salary > 150000) {
            printf("\nSalary you input is invalid. Please enter another salary: ");
            safe_flush();
        }
        
        printf("\nDo you want to update the employee with the following information:\n");
        printf("  %s %s, salary: %d \n", first_name, last_name, salary);
        printf("Enter 1 for yes, 0 for no: ");
        read_int(&is_confirm);
        printf("\n");
        
        if (is_confirm == 0) {
            printf("Don't want to update the employee in the DB.\n");
            return;
        }
        strcpy(employee[index].firstname, first_name);
        strcpy(employee[index].lastname, last_name);
        employee[index].salary = salary;
        
        printf("Updated Employee with id %d in DB \n", emp_id);
    }
}

void print_the_highest_salaries_employee(int emp_count, struct Employee employee[MAXEMPLOYEE]) {
    qsort(employee, emp_count, sizeof(struct Employee), compared_by_salary);
    int amount;
    printf("How many the highest salary employees you want to see: ");
    read_int(&amount);
    printf("\n");
    if (amount == -1) {
        printf("Invalid input! Please try again: ");
        safe_flush();
        read_int(&amount);
    }
    print_the_database(amount, employee);
    qsort(employee, emp_count, sizeof(struct Employee), compared_by_id);
}

void find_all_employees_with_matching_last_name(int emp_count, struct Employee employee[MAXEMPLOYEE]) {
    char lastname[MAXNAME];
    printf("Enter the last name of the employee you want to find: ");
    read_string(&lastname);
    
    if (search_by_lastName(employee, emp_count, lastname) == -1) {
        printf("Employee with last name %s not found in DB. \n", lastname);
        return;
    }
    
    int amount = 0;
    
    printf("\n");
    printf("NAME                              SALARY          ID\n");
    printf("---------------------------------------------------------------\n");
    for (int i = 0; i < emp_count; i++) {
        if (strcasecmp(employee[i].lastname, lastname) == 0) {
            printf("%-15s %-10s %13d %15d \n", employee[i].firstname, employee[i].lastname, employee[i].salary, employee[i].id);
            amount += 1;
        }
    }
    printf("---------------------------------------------------------------\n");
    printf(" Number of Employees (%d)\n", amount);
}