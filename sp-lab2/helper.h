#ifndef _LINK_HELPER_H_
#define _LINK_HELPER_H_

#include<stdio.h>
#include "employee.h"

/** Open a file with the file name
 * returns 0 on success, -1 on EOF
 */
int open_file(char *fname);

/** Read an int value from the user
 * returns 0 on success, -1 on EOF
 */
int read_int(int *address);

/** Read a float value from the use
 * returns 0 on success, -1 on EOF
*/
int read_float(float *address);

/** Read a string value from the use
 * returns 0 on success, -1 on EOF
*/
int read_string(char *address);

/** Close a file with the file name
 * returns 0 on success, -1 on EOF
*/
void close_file(FILE *fname);

/** Binary search
 * arr[]: an array need to search
 * l: the most left of the arr
 * r: the most right of the arr
 * target: the value we need to find in the arr
 * return: return the index of the value we need find in the arr, 
 * return -1 if we don't find it.
*/
int binary_search(const int arr[], int l, int r, int target);

/** Print an employee info by key
 * employee[MAXEMPLOYEE]: an employee struct array
 * key: the index of employee we want to print
*/
void print_by_key(struct Employee employee[MAXEMPLOYEE], int key);

/** Search an employee by last name
 * employee[MAXEMPLOYEE]: an employee struct array
 * n_emp: the count of employee's elements
 * surname: the employee's name we want to find
 * return: return the index of the value we need find in the arr, 
 * return -1 if we don't find it.
*/
int search_by_lastName(struct Employee employee[MAXEMPLOYEE], int n_emp, char lastname[]);

/** Compared by employee id
 * a: struct Employee
 * b: struct Employee
*/
int compared_by_id(const void *a, const void *b);

/** Compared by employee salary
 * a: struct Employee
 * b: struct Employee
*/
int compared_by_salary(const void *a, const void *b);

/** Compared by int
 * a: int
 * b: int
*/
int compare(const void* a, const void* b);

/**
 * Safe flush
*/
void safe_flush();

/**
 * Remove an element by index in an array
*/
void remove_element(int array[], int index, int array_length);

/**
 * Remove an employee by index in the employee array
*/
void remove_employee(struct Employee employee[MAXEMPLOYEE], int index, int emp_count);
#endif