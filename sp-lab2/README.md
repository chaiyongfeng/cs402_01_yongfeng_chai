# Systems Programming Lab #2 - C Programming Basics

## Environment

```
gcc 4.2.1
Apple clang version 11.0.3 (clang-1103.0.32.29)
```

## How to build an executable

```shell
gcc readfile.c employee.c helper.c constant.c -o workerDB
```

## How to execute

```shell
./workerDB input.txt
```

## Program main structure

### readfile

The main file `readfile` contains the main structure of the program.

```c
while(1) {
    print_option_info();
    int option;
    while (read_int(&option) == -1) {
        printf("Invalid input! Please try again: ");
        safe_flush();
    }
    printf("\n");

    if (option == 1) {
        print_the_database(emp_count, employee);
    } else if (option == 2) {
        lookup_by_id(emp_count, list, employee);
    } else if (option == 3) {
        lookup_by_lastname(emp_count, employee);
    } else if (option == 4) {
        add_an_employee(&emp_count, employee, list);
    } else if(option == 5) {
        quit();
        break;
    } else if (option == 6) {
        remove_an_employee(&emp_count, employee, list);
    } else if (option == 7) {
        update_an_employee(emp_count, employee, list);
    } else if (option == 8) {
        print_the_highest_salaries_employee(emp_count, employee);
    } else if (option == 9) {
        find_all_employees_with_matching_last_name(emp_count, employee);
    } else {
        printf("Hey, %d is not between 1 and 9, try again...\n", option);
        continue;
    }
}
```

### helper

The `helper` file abstracts some utility functions that will be used in `readfile` into it.

```c
/** Open a file with the file name
 * returns 0 on success, -1 on EOF
 */
int open_file(char *fname);

/** Read an int value from the user
 * returns 0 on success, -1 on EOF
 */
int read_int(int *address);

/** Read a float value from the use
 * returns 0 on success, -1 on EOF
*/
int read_float(float *address);

/** Read a string value from the use
 * returns 0 on success, -1 on EOF
*/
int read_string(char *address);

/** Close a file with the file name
 * returns 0 on success, -1 on EOF
*/
void close_file(FILE *fname);

/** Binary search
 * arr[]: an array need to search
 * l: the most left of the arr
 * r: the most right of the arr
 * target: the value we need to find in the arr
 * return: return the index of the value we need find in the arr, 
 * return -1 if we don't find it.
*/
int binary_search(const int arr[], int l, int r, int target);

/** Print an employee info by key
 * employee[MAXEMPLOYEE]: an employee struct array
 * key: the index of employee we want to print
*/
void print_by_key(struct Employee employee[MAXEMPLOYEE], int key);

/** Search an employee by last name
 * employee[MAXEMPLOYEE]: an employee struct array
 * n_emp: the count of employee's elements
 * surname: the employee's name we want to find
 * return: return the index of the value we need find in the arr, 
 * return -1 if we don't find it.
*/
int search_by_lastName(struct Employee employee[MAXEMPLOYEE], int n_emp, char lastname[]);

/** Compared by employee id
 * a: struct Employee
 * b: struct Employee
*/
int compared_by_id(const void *a, const void *b);

/** Compared by employee salary
 * a: struct Employee
 * b: struct Employee
*/
int compared_by_salary(const void *a, const void *b);

/** Compared by int
 * a: int
 * b: int
*/
int compare(const void* a, const void* b);

/**
 * Safe flush
*/
void safe_flush();

/**
 * Remove an element by index in an array
*/
void remove_element(int array[], int index, int array_length);

/**
 * Remove an employee by index in the employee array
*/
void remove_employee(struct Employee employee[MAXEMPLOYEE], int index, int emp_count);
```

### employee

Define the struct of `Employee`.

```c
struct Employee {
    char firstname[MAXNAME], lastname[MAXNAME];
    int id, salary;
};
```

### constant

Put all of the constant variables into this file.

```c
#define MAXNAME 64
#define MAXEMPLOYEE 1024
```