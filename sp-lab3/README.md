# Systems Programming Lab #3 - C Pointers

This is the cs402 sp-lab3 program that reads data from a file and computes some basic statistical analysis measures for the data.

The program works without any changes for data sets of any size, i.e. it would work for 10 data values or for 10 million without re-compilation.

## How to build an executable

```
gcc basicstats.c
```

## How to execute it.

```
./a.out small.txt
```

or 

```
./a.out large.txt
```

## Output

```c
> ./a.out small.txt
Results:
--------
Num values:        17
      mean:        102.799
    median:        71.808
    stddev:        99.560
Unused array capacity: 3
```

```c
./a.out large.txt
Results:
--------
Num values:        194
      mean:        1952.052
    median:        1854.000
    stddev:        1515.880
Unused array capacity: 126
```

## Debugging Memory Leaks & Errors

```c
> valgrind ./a.out large.txt -s                          master [ddbfe41] modified untracked
==27038== Memcheck, a memory error detector
==27038== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==27038== Using Valgrind-3.16.0.GIT and LibVEX; rerun with -h for copyright info
==27038== Command: ./a.out large.txt -s
==27038== 
Results:
--------
Num values:        194
      mean:        1952.052
    median:        1854.000
    stddev:        1515.880
Unused array capacity: 126
==27038== 
==27038== HEAP SUMMARY:
==27038==     in use at exit: 18,147 bytes in 162 blocks
==27038==   total heap usage: 182 allocs, 20 frees, 29,467 bytes allocated
==27038== 
==27038== LEAK SUMMARY:
==27038==    definitely lost: 0 bytes in 0 blocks
==27038==    indirectly lost: 0 bytes in 0 blocks
==27038==      possibly lost: 4,200 bytes in 5 blocks
==27038==    still reachable: 13,947 bytes in 157 blocks
==27038==         suppressed: 0 bytes in 0 blocks
==27038== Rerun with --leak-check=full to see details of leaked memory
==27038== 
==27038== For lists of detected and suppressed errors, rerun with: -s
==27038== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 4 from 4)
```