#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

/**
 * Open a file with read mode by the file path.
 * Return a pointer to FILE if the execution succeeds 
 * else NULL is returned.
 */
FILE *open_file(char *fname) {
    FILE *file = fopen(fname, "r");
    if (file == NULL) {
        printf("Occur an error when reading a file.");
        return 0;
    }
    return file;
}

/**
 * Get the average of values in the set.
 */
double get_mean(float *p, int n) {
    double sum = 0;
    for (int i = 0; i < n; i++) {
        sum = sum + p[i];
    }
    return sum / n;
}

/**
 * Get the population standard deviation by using the following formula:
 * stddev = sqrt((sum((xi - mean)^2))/N);
 */
double get_stddev(float *p, int n) {
    double sum = 0;
    double mean = get_mean(p, n);
    for (int i = 0; i < n; i++) {
        sum += pow(p[i] - mean, 2);
    }
    return sqrt(sum / n);
}

/**
 * Get the middle value in the set of values. 
 * If the set has an even number of values, 
 * average the two values that are left and right of center.
 */
double get_median(float *arr, int n) {
    int mid = n / 2;
    if (n % 2 != 0) {
        return arr[mid];
    } else {
        return (arr[mid-1] + arr[mid]) / 2;
    }
}

/**
 * Swap values pointed by xp and yp.
 */
void swap(float *xp, float *yp) {
    float temp = *xp;
    *xp = *yp;
    *yp = temp;
}

/**
 * Bubble sort.
 */
void sort(float *arr, int n) {
    for (int i = 0; i < n - 1; i++) {
        int is_sorted = 1;
        for (int j = 0; j < n - i - 1; j++) {
            if (arr[j + 1] > arr[j]) {
                swap(&arr[j + 1], &arr[j]);
                is_sorted = 0;
            }
        }
        if (is_sorted) break;
    }
}

/**
 * Read a file contains some numbers into an dynamic float array
 * allocated on the heap.
 * *n: start out allocating an array of n float values. If out of capacity,
 * expands twice the size of the current full one.
 * *length_arr: length of the array.
 */
float *read_file_into_dynamic_array(FILE *fp, int *n, int *length_arr) {
    float *arr = (float *)malloc(*n * sizeof(float));
    while (!feof(fp)) {
        fscanf(fp, "%f\n", arr + *length_arr);
        (*length_arr)++;
        if (*length_arr == *n) {
            float *new_arr = (float *)malloc((*n) * 2 * sizeof(float));
            memcpy(new_arr, arr, *length_arr * sizeof(float));
            free(arr);
            arr = new_arr;
            *n = (*n) * 2;
        }
    }
    return arr;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        printf("This program needs to pass a file name.\n");
        return 0;
    }
    int n = 20, lenght_arr = 0;
    char *fname = argv[1];
    FILE *fp = open_file(fname);

    float *arr = read_file_into_dynamic_array(fp, &n, &lenght_arr);

    fclose(fp);

    double mean = get_mean(arr, lenght_arr);
    sort(arr, lenght_arr);
    double median = get_median(arr, lenght_arr);
    double sd = get_stddev(arr, lenght_arr);

    printf("Results:\n");
    printf("--------\n");
    printf("Num values:        %d\n", lenght_arr);
    printf("      mean:        %0.3f\n", mean);
    printf("    median:        %0.3f\n", median);
    printf("    stddev:        %0.3f\n", sd);
    printf("Unused array capacity: %d\n", n - lenght_arr);
    free(arr);
    return 0;
}
