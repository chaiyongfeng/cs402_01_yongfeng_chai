.data 0x10000000
.align 0
ch1: .byte 'a'
word1: .word 0x89abcdef
ch2: .byte 'b'
word2: .word 0

.text
.globl main
main:
    la $a0, word1
    la $a1, word2
    lwl $t0, 3($a0)
    lwr $t0, 0($a0)
    swl $t0, 3($a1)
    swr $t0, 0($a1)
    jr $ra
