# this is a program used to test memory alignment for data 
            .data 0x10000000 
char1:      .byte 'a'           # reserve space for a byte
double1:    .double 1.1         # reserve space for a double
char2:      .byte 'b'           # b is 0x62 in ASCII
half1:      .half 0x8001        # reserve space for a half-word (2 bytes)
char3:      .byte 'c'           # c is 0x63 in ASCII
word1:      .word 0x56789abc    # reserve space for a word
char4:      .byte 'd'           # d is 0x64 in ASCII
word2:      .word 0
    
            .text
            .globl main 
main:
    la $a0, word1

    lb $t0, 3($a0) # load (use lb) the four bytes of word1 in successive registers starting with $t0
    lb $t1, 2($a0)
    lb $t2, 1($a0)
    lb $t3, 0($a0)

    lbu $t4, 3($a0) # load (use lbu) the four bytes of word1 in successive registers starting with $t4
    lbu $t5, 2($a0)
    lbu $t6, 1($a0)
    lbu $t7, 0($a0)

    lh $t8, half1($0)  # load (use lh) half1 in $t8
    lhu $t9, half1($0) # load (use lhu) half1 in $t9

# store the four bytes into word2 in reverse order: 
# the byte that was the most significant in word1 will be
# the least significant byte in word2, and so on.
    la $a0, word2
    sb $t3, 0($a0)  
    sb $t2, 1($a0)
    sb $t1, 2($a0)
    sb $t0, 3($a0)
    
    jr $ra
 
