# • prompts the user to enter an integer; 
#store the integer in memory in a variable called user1
# • calls a procedure named ‘Reverse_bytes’. 
# The argument passed to the procedure is the address of 
# the word whose bytes must be reversed. 
# The most significant byte will become the least significant and so on.
# • prints a message that reads “If bytes were layed 
# in reverse order the number would be: ”
# • prints the number whose bytes have been reversed

.data
user1: .word 0
msg: .asciiz "If bytes were layed in reverse order the number would be: "

.text
.globl main
main:
    addi $sp, $sp, -4      # substract 4 from stack pointer
    sw $ra, 4($sp)         # save $ra in allocated stack space

    li $v0, 5
    syscall

    sw $v0, user1($0)
    la $a0, user1
    jal Reverse_bytes

    li $v0, 4              # print message
    la $a0, msg
    syscall

    li $v0, 1              # print number
    lw $a0, user1($0)
    syscall
    
    lw $ra, 4($sp)         # restore $ra from stack
    addi $sp, $sp, 4       # free stack space
    jr $ra

Reverse_bytes:
    lb $t0, 0($a0)         # swap highest and lowest byte
    lb $t1, 3($a0)
    sb $t0, 3($a0)
    sb $t1, 0($a0)
    lb $t0, 1($a0)         # swap middle bytes
    lb $t1, 2($a0)
    sb $t0, 2($a0)
    sb $t1, 1($a0)   
    jr $ra
