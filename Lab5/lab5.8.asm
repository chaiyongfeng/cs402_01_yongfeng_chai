.data
word1: .word 0x89abcdef
# word1 in memory:
# 10010000       ef
# 10010001       cd
# 10010002       ab
# 10010003       89

.text
.globl main
main:
    la $a0, word1
    lwr $t4, 0($a0)
    lwr $t5, 1($a0)
    lwr $t6, 2($a0)
    lwr $t7, 3($a0)
    jr $ra