import time
import numpy as np

# measure performance between two systems

arr = []
for _ in range(5):
    start = time.process_time()
    a = np.random.rand(4000, 3000)
    b = np.random.rand(3000, 4000)
    c = np.dot(a, b)
    takeTime = time.process_time() - start
    arr.append(takeTime)
    # print(c)
    print('Time: ', takeTime)

print('Average: ', sum(arr) / len(arr))