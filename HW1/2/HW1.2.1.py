import time
import numpy as np

# test int

arr = []
for _ in range(5):
    start = time.process_time()
    a = np.random.randint(10, size=(400, 300))
    b = np.random.randint(10, size=(300, 400))
    c = np.dot(a, b)
    takeTime = time.process_time() - start
    arr.append(takeTime)
    print('Time: ', takeTime)
    
print('Average: ', sum(arr) / len(arr))