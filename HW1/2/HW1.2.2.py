import time
import numpy as np

# test double

arr = []
for _ in range(5):
    start = time.process_time()
    a = np.random.rand(400, 300)
    b = np.random.rand(300, 400)
    c = np.dot(a, b)
    takeTime = time.process_time() - start
    arr.append(takeTime)
    print('Time: ', takeTime)
    
print('Average: ', sum(arr) / len(arr))