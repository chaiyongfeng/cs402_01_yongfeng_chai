# • reserve space in memory for a variable called var1 of size word. The initial value of var1 will be the
# first digit of your SSN.
# • the program will implement the piece of C code described below.
#    for (i=var1; i<100; i++) {
#          var1 = var1+1;
# }

.data

var1: .word 6

.text
.globl main

main:
	lw $t1, var1          # $t1 <- var1
	move $t0, $t1         # $t0(i) <- $t1
	li $a1, 100           # $a1 <- 100
loop:
	ble $a1, $t0, exit    # if $t0 >= 100, exit loop
	addi $t1, $t1, 1      # var1 = var1 + 1
	addi $t0, $t0, 1      # i++
	j loop
exit:
	sw $t1, var1
	li $v0, 1             # syscall #1: print_int
	lw $a0, var1
	syscall
	jr $ra
