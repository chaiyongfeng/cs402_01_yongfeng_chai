# • reserve space in memory for an array of words of size 10. Use the ‘.space’ directive. The array is called
# my_array.
# • the program will implement the piece of C code described below. The value of initial_value is
# the first digit of your SSN. i and j will be in one of the registers $t0 to $t9.
#       j = initial_value;
#       for (i=0; i<10, i++) {
#             my_array[i] = j;
# j++;
# }

.data

my_array: .space 40            # reserve 40 bytes (10 words) for the array
initial_value: .word 6

.text
.globl main

main:
	lw $t1, initial_value     # $t1(j) <- initial_value
	la $t0, my_array          # $t0 <- start address of the array
	addi $a1, $t0, 40         # $a1 <- end address of array
loop:
	ble $a1, $t0 exit         # if $t0 >= $a1, exit loop 
	sw $t1, 0($t0)            # my_array[i] = j
	addi $t1, $t1, 1          # j++
	addi $t0, $t0, 4          # calculate address of next element
	j loop
exit:
	jr $ra

