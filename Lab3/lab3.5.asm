.data
msg0: .asciiz "Please enter an integer number: "
msg1: .asciiz "I'm far away"
msg2: .asciiz "I'm nearby"
far: .word 1

.text
.globl main
main:
    li $v0, 4
    la $a0, msg0          # syscall print msg0: Please enter an integer number:
    syscall

	li $v0, 5             # syscall read_int
	syscall
	move $t0, $v0         # move the value read into $t0

    li $v0, 4
    la $a0, msg0          # syscall print msg0: Please enter an integer number:
    syscall

	li $v0, 5
	syscall
	move $t1, $v0         # move the value read into $t1

	bne $t0, $t1, near    # if $t0 != $t1 go to near
	li $v0, 4             
	la $a0, msg1          # syscall print I'm far away
	syscall
    
	b far                 # branch unconditionally to 'far' label
near:
	li $v0, 4
	la $a0, msg2          # syscall print msgs: I'm nearby
	syscall
	jr $ra

