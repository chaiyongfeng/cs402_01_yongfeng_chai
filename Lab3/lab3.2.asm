.data

var1: .word 6     # var1 is a word(32 bits) with the initial value 6
var2: .word 2     # var2 is a word(32 bits) with the initial value 2
var3: .word -2020 # var3 is a word(32 bits) with the initial value -2020

.text
.globl main

main:
    lw $t1, var1       # $t1 <- var1
    lw $t2, var2       # $t2 <- var2
    slt $t0, $t1, $t2  # if $t1 < $t2: $t0 = 1
    blez $t0, Else     # if $t0 == 0: goto Else
    
    lw $t3, var3       # $t3 <- var3
    sw $t3, var1       # $t3 -> var1
    sw $t3, var2       # $t3 -> var2
    beq $t1, $t2, Exit # if $t1 == $t2: goto Exit

Else:
    la $t4, var1 # $t4 <- [M]var1
    la $t5, var2 # $t5 <- [M]var2

    # swap the value of var1 and var2
    sw $t1, 0($t5)
    sw $t2, 0($t4)

Exit:
    # print var1
    li $v0, 1
    lw $a0, var1
    syscall

    # print var2
    li $v0, 1
    lw $a0, var2
    syscall

    jr $ra