.data
msg: .asciiz "the largest number is: "
.text
.globl main
main:   li $v0, 5
        syscall         # read a number
        move $t0, $v0

        li $v0, 5
        syscall         # read a number
        move $t1, $v0

        addi $sp, $sp, -4 # substract 4 from stack pointer   
        sw $ra, 4($sp)    # save $ra in $sp
        addi $sp, $sp, -8 # allocate 8 bytes stack space 
        sw $t0, 4($sp)    # push arguments into stack
        sw $t1, 8($sp)
        jal Largest       # call Largetst
        addi $sp, $sp, 8
        lw $ra, 4($sp)    # restore $ra from stack
        addi $sp, $sp, 4
        jr $ra            # return from main

Largest:    
    li $v0, 4
    la $a0, msg
    syscall             # print message

    lw $a0, 4($sp)
    lw $a1, 8($sp)
    slt $t0, $a0, $a1   # if $a0 < $a1: $t0 = 1
    blez, $t0, output   # if $t0 <= 0: go to output
    move $a0, $a1

output:
    li $v0, 1
    syscall     # print largest number
    jr $ra
