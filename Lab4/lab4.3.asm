.data
msg: .asciiz "the largest number is: "
.text
.globl main
main:   li $v0, 5
        syscall         # read a number
        move $t0, $v0

        li $v0, 5
        syscall         # read a number
        move $t1, $v0

        addi $sp, $sp, -4 # substract 4 from stack pointer   
        sw $ra, 4($sp)    # save $ra in $sp
        move $a0, $t0     # pass parameters
        move $a1, $t1
        jal Largest       # call Largetst
        lw $ra, 4($sp)    # restore $ra from stack
        addi $sp, $sp, 4
        jr $ra            # return from main

Largest:    
    move $t0, $a0
    li $v0, 4
    la $a0, msg
    syscall             # print message

    move $a0, $t0
    slt $t0, $a0, $a1   # if $a0 < $a1: $t0 = 1
    blez, $t0, output   # if $t0 <= 0: go to output
    move $a0, $a1

output:
    li $v0, 1
    syscall     # print largest number
    jr $ra
