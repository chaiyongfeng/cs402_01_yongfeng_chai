.data
input_msg: .asciiz "Enter two non-negative integers: "
output_msg: .asciiz "Answer is: "
err_msg: .asciiz "One or more number(s) is negative. Try again.\n"

.text
.globl main
main:
    li $v0, 4
    la $a0, input_msg
    syscall
    li $v0, 5           # read a number
    syscall
    move $t0, $v0
    li $v0, 5           # read another number
    syscall
    move $t1, $v0
    bltz $t0, err       # check whether input is negative
    bltz $t1, err
    j ok                # if not negative, go to ok
err:
    li $v0, 4           # print error message and try again
    la $a0, err_msg
    syscall
    j main
ok:
    li $v0, 4
    la $a0, output_msg  # print output_msg
    syscall

    addi $sp, $sp, -4   # allocate 4 space in $sp
    sw $ra, 4($sp)      # save $ra
    move $a0, $t0       # pass parameters
    move $a1, $t1
    jal Ackermann       # go to Ackermann
    move $a0, $v0
    li $v0, 1
    syscall             # print answer
    lw $ra, 4($sp)      # restore $ra
    addi $sp, $sp, 4
    jr $ra

Ackermann:
    beq $a0, $0, x_0
    addi $sp, $sp, -4   
    sw $ra, 4($sp)
    beq $a1, $0, y_0
    addi $t0, $a0, -1
    addi $sp, $sp, -4
    sw $t0, 4($sp)      # save x - 1 in stack
    addi $a1, $a1, -1
    jal Ackermann       # A(x, y - 1)
    lw $a0, 4($sp)
    addi $sp, $sp, 4
    move $a1, $v0
    jal Ackermann       # A(x - 1, A(x, y - 1))
    j end
y_0:                    # y == 0
    addi $a0, $a0, -1
    li $a1, 1
    jal Ackermann       # A(x - 1, 1)
end:
    lw $ra, 4($sp)
    addi $sp, $sp, 4
    jr $ra
x_0:
    addi $v0, $a1, 1
    jr $ra


