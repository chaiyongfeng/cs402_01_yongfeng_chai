.data 0x10010000

var1: .word 62 # var1 is a word(32 bits) with the initial value 62
var2: .word 21 # var1 is a word(32 bits) with the initial value 21

.extern ext1 4 # ext1 stored at sym is 4 bytes and is a global symbol
.extern ext2 4 # ext2 stored at sym is 4 bytes and is a global symbol

.text
.globl main

main:

    lw $t1, var1 # $t1 = M[var1]
    lw $t2, var2 # $t2 = M[var2]

    la $t0, ext2   # $t0 = Address of ext2
    sw $t1, 0($t0) # M[$t0 + 0] = $t1
    la $t0, ext1   # $t0 = Address of ext1
    sw $t2, 0($t0) # M[$t0 + 0] = $t2

    # Test ext1, ext2
    # li $v0, 4
    # la $a0, ext1
    # syscall

    # li $v0, 4
    # la $a0, ext1
    # syscall

    li $v0, 10 # syscall exit
    syscall

    