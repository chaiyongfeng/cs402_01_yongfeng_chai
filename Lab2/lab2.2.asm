.data 0x10010000
var1: .word 83  # var1 is a word(32 bits) with the initial value 83
var2: .word 104 # var1 is a word(32 bits) with the initial value 104
var3: .word 111 # var1 is a word(32 bits) with the initial value 111
var4: .word 119 # var1 is a word(32 bits) with the initial value 119

first: .asciiz "y" # first is a string with the initial value y
last: .asciiz "c"  # first is a string with the initial value c

.text
.globl main

main:
        la $t1, var1 # $t1 = M[var1]
        lw $t2, var2 # $t1 = M[var2]
        lw $t3, var3 # $t1 = M[var3]
        lw $t4, var4 # $t1 = M[var3]

        # la $t5, first
        # la $t6, last

        move $t0, $t1 # move the contents of $t1 to $t0
        move $t7, $t2 # move the contents of $t2 to $t7

        move $t1, $t4 # move the contents of $t4 to $t1
        move $t2, $t3 # move the contents of $t3 to $t2
        move $t3, $t7 # move the contents of $t7 to $t3
        move $t4, $t0 # move the contents of $t0 to $t4

        li $v0, 10 # syscall exit
        syscall