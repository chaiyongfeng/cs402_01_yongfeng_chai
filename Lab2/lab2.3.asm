.data 0x10010000
var1: .word 83  # var1 is a word(32 bits) with the initial value 83
var2: .word 104 # var1 is a word(32 bits) with the initial value 104
var3: .word 111 # var1 is a word(32 bits) with the initial value 111
var4: .word 119 # var1 is a word(32 bits) with the initial value 119

first: .asciiz "y" # first is a string with the initial value y
last: .asciiz "c"  # first is a string with the initial value c

.text
.globl main

main: 
        lui $1, 0x1001
        lw $t1, 0($1)    # store the value of variable "var1" to $t1
        lui $1, 0x1001
        lw $t2, 4($1)    # store the value of variable "var2" to $t2
        lui $1, 0x1001
        lw $t3, 8($1)    # store the value of variable "var3" to $t3
        lui $1, 0x1001
        lw $t4, 12($1)   # store the value of variable "var4" to $t4

        # la $t5, first
        # la $t6, last

        addu $t0, $0, $t1 # $t0 = $t1
        addu $t7, $0, $t2 # $t7 = $t2

        addu $t1, $0, $t4 # $t1 = $t4
        addu $t2, $0, $t3 # $t2 = $t3
        addu $t3, $0, $t7 # $t3 = $t7
        addu $t4, $0, $t0 # $t4 = $t0

        ori $2, $0, 10 # syscall exit
        syscall