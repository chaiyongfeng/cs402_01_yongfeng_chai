#ifndef _LINK_EMPLOYEE_H
#define _LINK_EMPLOYEE_H

#include "constant.h"

struct Employee {
    char firstname[MAXNAME], lastname[MAXNAME];
    int id, salary;
};

#endif // !_LINK_EMPLOYEE_H

