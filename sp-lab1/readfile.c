#include "readfile.h"
#include <stdlib.h>
#include "helper.h"

/* Print the option info */
void print_option_info();

/* Print the Database */
void print_the_database(int emp_count, struct Employee employee[MAXEMPLOYEE]);

/* Lookup employee by ID */
void lookup_by_id(int emp_count, int list[MAXEMPLOYEE], struct Employee employee[MAXEMPLOYEE]);

/* Lookup employee by last name */
void lookup_by_lastname(int emp_count, struct Employee employee[MAXEMPLOYEE]);

/* Add an Employee */
void add_an_employee(int *emp_count, struct Employee employee[MAXEMPLOYEE], int list[MAXEMPLOYEE]);

/* Quit */
void quit();

int main(int argc, char const *argv[]) {
    if (argc < 2) {
        printf("Need pass a filename to read...\n");
        return 0;
    }

    char *fname = argv[1];
    struct Employee employee[MAXEMPLOYEE];
    int emp_count = 0, list[MAXEMPLOYEE];

    if (open_file(fname) == -1) {
        printf("Occur an error when reading this file!");
        return -1;
    }

    printf("\nfname: %s\n", fname);
    FILE *fp = fopen(fname, "r");
    while(!feof(fp)) {
        fscanf(fp, "%d %s %s %d\n", &employee[emp_count].id, &employee[emp_count].firstname, &employee[emp_count].lastname, &employee[emp_count].salary);
        list[emp_count] = employee[emp_count].id;
        emp_count++;
    }

    qsort(employee, emp_count, sizeof(struct Employee), compared_by_id);
    qsort(list, emp_count, sizeof(int), compare);

    close_file(fp);

    while(1) {
        print_option_info();
        int option;
        while (read_int(&option) == -1) {
            printf("Invalid input! Please try again: ");
            safe_flush();
        }
        printf("\n");
    
        if (option == 1) {
            print_the_database(emp_count, employee);
        } else if (option == 2) {
            lookup_by_id(emp_count, list, employee);
        } else if (option == 3) {
            lookup_by_lastname(emp_count, employee);
        } else if (option == 4) {
            add_an_employee(&emp_count, employee, list);
        } else if(option == 5) {
            quit();
            break;
        } else {
            printf("Hey, %d is not between 1 and 5, try again...\n", option);
            continue;
        }
    }
}

void print_option_info() {
    printf("\n");
    printf("Employee DB Menu:\n");
    printf("----------------------------------\n");
    printf("  (1) Print the Database\n");
    printf("  (2) Lookup by ID\n");
    printf("  (3) Lookup by Last Name\n");
    printf("  (4) Add an Employee\n");
    printf("  (5) Quit\n");
    printf("----------------------------------\n");
    printf("Enter your choice: ");
}

void print_the_database(int emp_count, struct Employee employee[MAXEMPLOYEE]) {
    printf("\n");
    printf("NAME                              SALARY 	     ID\n");
    printf("---------------------------------------------------------------\n");
    for (int i = 0; i < emp_count; i++) {
        printf("%-15s %-10s %13d %15d \n", employee[i].firstname, employee[i].lastname, employee[i].salary, employee[i].id);
    }
    printf("---------------------------------------------------------------\n");
    printf(" Number of Employees (%d)\n", emp_count);
};

void lookup_by_id(int emp_count, int list[MAXEMPLOYEE], struct Employee employee[MAXEMPLOYEE]) {
    int emp_id, key;
    printf("Enter a 6 digit employee id: ");
    while (read_int(&emp_id) == -1) {
        printf("Invalid input! Please try again: ");
        safe_flush();
    }
    printf("\n");
    key = binary_search(list, 0, emp_count, emp_id);
    if (key == -1) {
        printf("Employee with id %d not found in DB \n", emp_id);
    } else {
        print_by_key(employee, key);
    }
}

void lookup_by_lastname(int emp_count, struct Employee employee[MAXEMPLOYEE]) {
    char lastname[MAXNAME];
    int key;
    printf("Enter Employee's lastname (no extra spaces): ");
    read_string(&lastname);
    printf("\n");
    key = search_by_lastName(employee, emp_count, lastname);
    if (key == -1) {
        printf("Employee with lastname %s not found in DB. \n", lastname);
    } else {
        print_by_key(employee, key);
    }
}

void add_an_employee(int *emp_count, struct Employee employee[MAXEMPLOYEE], int list[MAXEMPLOYEE]) {
    char firstname[MAXNAME], lastname[MAXNAME];
    int salary, is_confirm;
    
    printf("Enter the firstname of the employee: ");
    read_string(&firstname);
    printf("\n");
    printf("Enter the lastname of the employee: ");
    read_string(&lastname);
    printf("\n");
    printf("Enter employee's salary (30000 to 150000): ");
    
    while (read_int(&salary) == -1 || salary < 30000 || salary > 150000) {
        printf("\nSalary you input is invalid. Please enter another salary: ");
        safe_flush();
    }

    printf("\n");
    printf("Do you want to add the following employee to the DB?\n");
    printf("  %s %s, salary: %d\n", firstname, lastname, salary);
    printf("\n");
    printf("Enter 1 for yes, 0 for no: ");
    while (read_int(&is_confirm) == -1) {
        printf("Invalid input! Please try again: ");
        safe_flush();
    }
    printf("\n");
    if (is_confirm) {
        int cur_id = employee[*emp_count - 1].id;
        int new_id = cur_id + 1;
        strcpy(&employee[*emp_count].firstname, firstname);
        strcpy(&employee[*emp_count].lastname, lastname);
        employee[*emp_count].salary = salary;
        employee[*emp_count].id = new_id;
        list[*emp_count] = employee[*emp_count].id;
        *emp_count = *emp_count + 1;
        printf("Added!\n");
    } else {
        printf("Cancelled!.\n");
    }
}

void quit() {
    printf("goodbye!\n");
}