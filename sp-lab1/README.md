# Systems Programming Lab #1 - C Programming Basics

## How to build an executable

```shell
gcc readfile.c employee.c helper.c constant.c -o workerDB
```

## How to execute

```shell
./workerDB input.txt
```