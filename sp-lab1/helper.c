#include "helper.h"
#include <string.h>

int open_file(char *fname) {
    if(fopen(fname, "r") == NULL)
       return -1;
    return 0;
}

int read_int(int *address) {
    if(scanf("%d", address) == 0)
       return -1;
    return 0;
}

int read_float(float *address) {
    if(scanf("%f", address) == 0)
       return -1;
    return 0;
}

int read_string(char *address) {
    if(scanf("%s", address) == 0)
       return -1;
    return 0;
}

void close_file(FILE *fname) {
    fclose(fname);
}

int binary_search(const int arr[], int l, int r, int target) {
    if (r >= l) {
        int mid = l + (r - l) / 2;
        if (arr[mid] == target) {
            return mid;
        }
        
        if (arr[mid] > target) {
            return binary_search(arr, l, mid - 1, target);
        }

        return binary_search(arr, mid + 1, r, target);
    }
    return -1;
}

void print_by_key(struct Employee employee[MAXEMPLOYEE], int key) {
    printf("NAME                              SALARY 	     ID\n");
    printf("---------------------------------------------------------------\n");
    printf("%-10s %-10s %17d %17d \n", employee[key].firstname, employee[key].lastname, employee[key].salary, employee[key].id);
    printf("---------------------------------------------------------------\n");
}

int search_by_lastName(struct Employee employee[MAXEMPLOYEE], int n_emp, char lastname[]) {
    for (int i = 0; i < n_emp; i++){
        if(strcasecmp(employee[i].lastname, lastname) == 0) {
            return i;
        }
    }
    return -1;
}

int compared_by_id(const void *a, const void *b) {
    struct Employee *employee1 = (struct Employee *)a;
    struct Employee *employee2 = (struct Employee *)b;
    return (*employee1).id - (*employee2).id;
}

int compare( const void* a, const void* b) {
    int int_a = * ( (int*) a );
    int int_b = * ( (int*) b );
    return int_a - int_b;
}

void safe_flush() {
    char c;
    while((c = getchar()) != '\n' && c != EOF);
}